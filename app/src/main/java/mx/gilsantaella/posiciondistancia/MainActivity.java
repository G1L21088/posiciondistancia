package mx.gilsantaella.posiciondistancia;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {

    private EditText edtLongitud;
    private EditText edtLatitud;
    private TextView txtDistancia;
    private Button btnCalcular;
    private Ubicacion ub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setCasting();
    }

    private void setCasting() {
        edtLatitud = (EditText) findViewById(R.id.edtLatitud);
        edtLongitud = (EditText) findViewById(R.id.edtLongitud);
        txtDistancia = (TextView) findViewById(R.id.txtDistancia);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        ub = new Ubicacion(this);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDistancia.setText(obtenerDistancia());
            }
        });
    }

    private String obtenerDistancia(){
        StringBuilder str = new StringBuilder();
        double distancia = 0;
        String direccion = null;

        DecimalFormat decimales = new DecimalFormat("0.00");

        if(edtLongitud.length() != 0 && edtLatitud.length() != 0){
            double x1 = Double.parseDouble(String.valueOf(edtLongitud.getText()));
            double y1 = Double.parseDouble(String.valueOf(edtLatitud.getText()));
            double x2 = ub.getLongitud();
            double y2 = ub.getLatitud();

            Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = gcd.getFromLocation(y1,
                        x1, 1);
                if (addresses.size() > 0)
                    System.out.println(addresses.get(0).getLocality());
                direccion = addresses.get(0).getAddressLine(0);
            }
            catch (IOException e) {
                e.printStackTrace();
            }


            distancia = (Math.sqrt((Math.pow((x2-x1), 2)) + (Math.pow((y2-y1), 2))));
            distancia = distancia * 100;
        }

        str
                .append("Longitud: ").append(ub.getLongitud())
                .append("\nLatitud: ").append(ub.getLatitud())
                .append("\n").append(ub.getDireccion())
                .append("\nDistancia: ").append(decimales.format(distancia)). append(" km")
                .append("\n").append(direccion);

        return str.toString();
    }

}
