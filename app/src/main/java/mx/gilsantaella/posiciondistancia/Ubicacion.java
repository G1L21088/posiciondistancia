package mx.gilsantaella.posiciondistancia;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Infofast on 21/01/2016.
 */

public class Ubicacion implements LocationListener {

    private Context context;
    private LocationManager locationManager;
    private String provider;
    private boolean red;
    private double Longitud;
    private double Latitud;

    public double getLongitud() {
        return Longitud;
    }

    public double getLatitud() {
        return Latitud;
    }

    public Ubicacion(Context context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        provider = LocationManager.NETWORK_PROVIDER;
        red = locationManager.isProviderEnabled(provider);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(provider, 1000, 1, this);
        setLocation();
    }

    public void setLocation() {
        if (red) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Location loc = locationManager.getLastKnownLocation(provider);
            if(loc != null){
                Longitud = loc.getLongitude();
                Latitud = loc.getLatitude();
            } else {
                Longitud = 0;
                Latitud = 0;
            }
        }
    }

    public String getDireccion(){
        String direccion = null;
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(Latitud,
                    Longitud, 1);
            if (addresses.size() > 0)
                System.out.println(addresses.get(0).getLocality());
            direccion = addresses.get(0).getAddressLine(0);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder s = new StringBuilder();
                s.append("Ubicación: ").append(direccion);
        return s.toString();
    }

    @Override
    public void onLocationChanged(Location location) {
        setLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

